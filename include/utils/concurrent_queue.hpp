//==============================================================================
//
//       Filename:  ConcurrentQueue.hpp
//
//    Description:  lock-free queue with optional waiting
//
//        Version:  1.0
//        Created:  12/08/2015 05:35:57 PM
//       Revision:  none
//       Compiler:  clang++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================


#ifndef  concurrent_queue_HPP
#define  concurrent_queue_HPP

#include <exception>
#include <string>
#include <memory>
#include <atomic>
#include <future>

namespace space_flow {
namespace utils {

// =============================================================================
//        Class:  EmptyQueueException
//  Description:  exception to be thrown when empty
// =============================================================================
class EmptyQueueException : public std::exception {
public:
  explicit EmptyQueueException(std::string msg = "EmptyQueueException");
  const char* what() const noexcept override;
protected:
  std::string message_;
}; // -----  end of class EmptyQueueException  -----


// =============================================================================
//        Class:  ConcurrentQueue
//  Description:  Lock-free concurrent queue
// =============================================================================
template <class T> class ConcurrentQueue {
public:
  // ====================  LIFECYCLE     =======================================
  ConcurrentQueue()
      : head_{std::make_shared<node>()},
        tail_{head_}{}

  ConcurrentQueue(const ConcurrentQueue& other) = delete;
  ConcurrentQueue(ConcurrentQueue&& other) = delete;

  // ====================  ACCESSORS     =======================================
  bool empty() const { return atomic_load(&head_) == atomic_load(&tail_); }

  // ====================  MUTATORS      =======================================
  T pop() {
    while (true) {
      auto first = atomic_load(&head_); // previously popped
      auto last = atomic_load(&tail_);
      auto next = atomic_load(&first->next); // who to pop

      if (first != last) {
        if (atomic_compare_exchange_weak(&head_, &first, next))
          return move(*next->value);
      } else {
        if (next == nullptr)
          throw EmptyQueueException{};
        else
          atomic_compare_exchange_weak(&tail_, &last, move(next));
      }
    }
  }

  template <class... Args> void push(Args&&... args) {
    auto new_node = std::make_shared<node>(std::forward<Args>(args)...);
    std::shared_ptr<node> expected_next;
    std::shared_ptr<node> last;

    while (true) {
      expected_next.reset();
      last = atomic_load(&tail_);

      if (atomic_compare_exchange_weak(&last->next, &expected_next, new_node)) {
        atomic_compare_exchange_strong(&tail_, &last, move(new_node));
        return;
      } else { // expected_next loaded last->next != nullptr
        if (expected_next != nullptr) // might have failed spuriously
          atomic_compare_exchange_weak(&tail_, &last, move(expected_next));
      }
    }
  }

  // ====================  OPERATORS     =======================================
  ConcurrentQueue& operator=(const ConcurrentQueue& other) = delete;
  ConcurrentQueue& operator=(ConcurrentQueue&& other) = delete;

private:
  // ====================  INNER CLASSES =======================================
  class node{
  public:
    node() = default;

    template <class... Args>
    explicit node(Args&&... args)
        : value{std::make_unique<T>(std::forward<Args>(args)...)} {}

    std::unique_ptr<T> value;
    std::shared_ptr<node> next = nullptr;
  };

  // ====================  DATA MEMBERS  =======================================
  std::shared_ptr<node> head_;
  std::shared_ptr<node> tail_;

}; // -----  end of template class ConcurrentQueue  -----

}  // -----  end of namespace utils  ----- 
}  // -----  end of namespace space_flow  ----- 

#endif   // ----- #ifndef concurrent_queue_HPP  ----- 
