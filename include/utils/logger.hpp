//==============================================================================
//
//       Filename:  logger.hpp
//
//    Description:  logger facility
//
//        Version:  1.0
//        Created:  12/08/2015 06:38:49 PM
//       Revision:  none
//       Compiler:  clang++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================


#ifndef  logger_HPP
#define  logger_HPP

#include <ostream>
#include "concurrent_monitor.hpp"

namespace space_flow {
namespace utils {

class Logger {
public:
  static ConcurrentMonitor<std::ostream&>& get();
};


} // -----  end of namespace utils  -----
}  // -----  end of namespace space_flow  ----- 


#endif   // ----- #ifndef logger_HPP  ----- 
