//==============================================================================
//
//       Filename:  ConcurrentMonitor.hpp
//
//    Description:  concurrent message-passing for objects
//
//        Version:  1.0
//        Created:  12/08/2015 05:59:48 PM
//       Revision:  none
//       Compiler:  clang++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <iostream>

#include "utils/concurrent_queue.hpp"

namespace space_flow {
namespace utils {

// =============================================================================
//        Class: ConcurrentMonitor
//  Description: Provides a thread-safe message-passing interface to monitored
//               object
// =============================================================================
template <class T> class ConcurrentMonitor {
private:
  
  bool stop_;
  T object_;
  mutable std::mutex mutex_;
  mutable std::condition_variable cv_;
  mutable ConcurrentQueue<std::function<void()> > queue_;
  std::thread worker_;

public:
  ConcurrentMonitor(T t)
      : stop_{false},
        object_{t},
        worker_{[this]() {
          while (!stop_)
            wait_and_pop()();
        }} {}

  ~ConcurrentMonitor() {
    queue_.push([this]() { stop_ = true; });
    refresh_barrier();
    worker_.join();
  }

  template <class Function>
  auto operator()(Function f) const -> std::future<decltype(f(object_))> {

    // std::function has to be copyable, so we must use shared pointers instead
    // of unique_ptr
    auto promise = std::make_shared<std::promise<decltype(f(object_))> >();
    auto fut = promise->get_future();

    queue_.push([ promise = move(promise), f = std::move(f), this ]() {
      try {
        set_value(*promise, f, object_);
      } catch (...) {
        promise->set_exception(std::current_exception());
      }
    });
    refresh_barrier();

    return fut;
  }

private:
  void refresh_barrier() const {
    std::lock_guard<std::mutex> _{mutex_};
    cv_.notify_one();
  }

  std::function<void()> wait_and_pop() const {
    std::unique_lock<std::mutex> lock{mutex_};
    while(true){
      try {
        return queue_.pop();
      } catch (EmptyQueueException&) {
        cv_.wait(lock);
      }
    }
  }

  template <class Return, class Function>
  void set_value(std::promise<Return>& promise, Function& f, T& t) const {
    promise.set_value(f(t));
  }

  template <class Function>
  void set_value(std::promise<void>& promise, Function& f, T& t) const {
    f(t);
    promise.set_value();
  }
};

} // -----  end of namespace utils  -----
} // -----  end of namespace space_flow  -----
