//==============================================================================
//
//       Filename:  glfw_guard.hpp
//
//    Description:  raii wrapper for glfw context
//
//        Version:  1.0
//        Created:  12/08/2015 05:18:45 PM
//       Revision:  none
//       Compiler:  clang++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#ifndef glfw_guard_HPP
#define glfw_guard_HPP

namespace space_flow {
namespace utils {

// =============================================================================
//        Class:  GlfwWrapper
//  Description:  automatically create and destroy glfw context
// =============================================================================
class GlfwWrapper {
public:
  // ====================  LIFECYCLE     =======================================
  GlfwWrapper(); // constructor
  ~GlfwWrapper(); // destructor
  GlfwWrapper& operator=(const GlfwWrapper&) = delete;

private:

  static void error_callback(int error, const char* description);

  static bool init_;
}; // -----  end of class GlfwWrapper  -----

} // -----  end of namespace utils  -----
} // -----  end of namespace space_flow  -----

#endif // ----- #ifndef glfw_guard_HPP  -----
