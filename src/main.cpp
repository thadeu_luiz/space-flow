//==============================================================================
//
//       Filename:  main.cpp
//
//    Description:  main
//
//        Version:  1.0
//        Created:  12/08/2015 06:58:04 PM
//       Revision:  none
//       Compiler:  clang++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <iostream>
#include <array>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "utils/logger.hpp"
#include "utils/glfw_guard.hpp"

using namespace std;
using namespace space_flow::utils;

int main(){
  Logger::get()([](auto& os) { os << "Hello!" << endl; });
  GlfwWrapper glfw_guard{};

  GLFWwindow* window =
      glfwCreateWindow(640, 480, "Hello Triangle", nullptr, nullptr);
  if(window==nullptr){
    cout << "Error: could not create window" << endl;
    return 1;
  }

  glfwMakeContextCurrent(window);
  glewExperimental = true;
  glewInit();

  cout << "Renderer: " <<glGetString(GL_RENDERER) << endl;
  cout << "Version: " <<glGetString(GL_VERSION) << endl;

  glEnable (GL_DEPTH_TEST);
  glDepthFunc (GL_LESS);

  const std::array<float, 9> points = {{0.0f, 0.5f,  0.0f,  0.5f, -0.5f,
                                 0.0f, -0.5f, -0.5f, 0.0f}};

  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * points.size(), points.data(),
               GL_STATIC_DRAW);

  GLuint vao = 0;
  glGenVertexArrays (1, &vao);
  glBindVertexArray (vao);
  glEnableVertexAttribArray (0);
  glBindBuffer (GL_ARRAY_BUFFER, vbo);
  glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

  const char* vertex_shader = "#version 130\n"
                              "in vec3 vp;"
                              "void main () {"
                              "  gl_Position = vec4 (vp, 1.0);"
                              "}";

  const char* fragment_shader = "#version 130\n"
                                "out vec4 frag_colour;"
                                "void main () {"
                                "  frag_colour = vec4 (0.5, 0.7, 0.5, 1.0);"
                                "}";

  GLuint vs = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vs, 1, &vertex_shader, NULL);
  glCompileShader(vs);
  GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fs, 1, &fragment_shader, NULL);
  glCompileShader(fs);

  GLuint shader_programme = glCreateProgram();
  glAttachShader(shader_programme, fs);
  glAttachShader(shader_programme, vs);
  glLinkProgram(shader_programme);

  while (!glfwWindowShouldClose(window)) {
    // wipe the drawing surface clear
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(shader_programme);
    glBindVertexArray(vao);

    // draw points 0-3 from the currently bound VAO with current in-use shader
    glDrawArrays(GL_POINTS, 0, 3);
    // update other events like input handling
    glfwPollEvents();
    // put the stuff we've been drawing onto the display
    glfwSwapBuffers(window);
  }

  return 0;
}
