//==============================================================================
//
//       Filename:  EmptyQueueException.cpp
//
//    Description:  definition for exception
//
//        Version:  1.0
//        Created:  12/08/2015 05:39:00 PM
//       Revision:  none
//       Compiler:  clang++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <exception>
#include <string>

#include "utils/concurrent_queue.hpp"

using namespace std;

namespace space_flow {
namespace utils {

EmptyQueueException::EmptyQueueException(string msg)
    : message_{move(msg)} {}

const char* EmptyQueueException::what() const noexcept {
  return message_.c_str();
}

}  // -----  end of namespace utils  ----- 
}  // -----  end of namespace space_flow  ----- 
