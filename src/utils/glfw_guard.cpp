//==============================================================================
//
//       Filename:  glfw_guard.cpp
//
//    Description:  glfw_guard implementation
//
//        Version:  1.0
//        Created:  12/08/2015 05:24:28 PM
//       Revision:  none
//       Compiler:  clang++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <stdexcept>

#define GLFW_DLL
#include "GLFW/glfw3.h"

#include "utils/glfw_guard.hpp"
#include "utils/logger.hpp"

using namespace std;

namespace space_flow {
namespace utils {


GlfwWrapper::GlfwWrapper() {
  if (init_)
    throw runtime_error{"ERROR: GLFW is already initialized"};
  if (!glfwInit()) {
    throw runtime_error{"ERROR: Failed to init GLFW3"};
  }
  init_ = true;
  glfwSetErrorCallback(GlfwWrapper::error_callback);
}

GlfwWrapper::~GlfwWrapper() {
  glfwTerminate();
  init_ = false;
}

bool GlfwWrapper::init_ = false;

void GlfwWrapper::error_callback(int error, const char* description) {
  string message{description};
  Logger::get()([ error, message = move(message) ](auto& os) {
    os << "GLFW ERROR: error code: " << error << endl
       << "description: " << message << endl;
  });
}

} // -----  end of namespace utils  -----
} // -----  end of namespace space_flow  -----
