//==============================================================================
//
//       Filename:  logger.cpp
//
//    Description:  Logger class definitions
//
//        Version:  1.0
//        Created:  12/08/2015 06:41:00 PM
//       Revision:  none
//       Compiler:  clang++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <fstream>
#include <ostream>
#include <chrono>

#include "utils/logger.hpp"

using namespace std;
using namespace chrono;

namespace space_flow {
namespace utils {
  ConcurrentMonitor<ostream&>& Logger::get(){
    static fstream log_stream{
        "logfile_" + to_string(system_clock::to_time_t(system_clock::now())) +
            ".log",
        ios::out};
    static ConcurrentMonitor<ostream&> monitor{log_stream};

    return monitor;
  }
}  // -----  end of namespace utils  ----- 
}  // -----  end of namespace space_flow  ----- 
